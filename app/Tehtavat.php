<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tehtavat extends Model
{
    protected $primaryKey = 'Teht_id';
    protected $table = 'Tehtavat';
    protected $fillable = ['otsikko'];
    public $timestamps = false;

    public function tehtavat() {
        return $this->hasOne('app/Tehtavat', 'Tila_id', 'Teht_id');
    }
}
