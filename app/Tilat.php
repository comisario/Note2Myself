<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tilat extends Model
{
    protected $primaryKey = 'Tila_id';
    protected $table = 'Tilat';
    protected $fillable = ['tila_tila'];
    public $timestamps = false;

    public function tilat() {
        return $this->hasOne('app/Tilat', 'Teht_id', 'Tila_id');
    }
}
