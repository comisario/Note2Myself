<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teht_aika extends Model
{
    protected $primaryKey = 'Aika_id';
    protected $table = 'Teht_aika';
    protected $fillable = ['paivamaara'];
    public $timestamps = false;

    public function aika() {
        return $this->hasOne('app/Tehtavat', 'Teht_id', 'Aika_id');
    }
}
