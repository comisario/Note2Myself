<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Tehtavat;
use App\Teht_aika;
use App\Tilat;

class ApiController extends Controller
{
    /**
     * Returns all notes from database or a note by id, title or date.
     *
     * @param $request
     * @return Response
     */
    public function showNotes(Request $request){

        $id = $request->input('id');
        $otsikko = $request->input('title');
        $paiva = $request->input('date');

        $idExists = Tehtavat::where('teht_id', $id)
            ->first();

        $otsikkoExists = Tehtavat::where('otsikko', $otsikko)
            ->first();

        $paivaExists = Teht_aika::where('paivamaara', $paiva)
            ->first();

        if($idExists != null || $otsikkoExists != null || $paivaExists != null) {
            $tehtavaList = Tehtavat::where('teht_id', $id)
                ->orWhere('otsikko', $otsikko)
                ->orWhere('teht_aika.paivamaara', $paiva)
                ->join('teht_aika', 'teht_id', '=', 'teht_aika.aika_id')
                ->join('tilat', 'teht_id', '=', 'tilat.tila_id')
                ->select('teht_id', 'otsikko', 'teht_aika.paivamaara', 'tilat.tila_tila')
                ->get();

            return response()->json($tehtavaList, 200);

        }else if ($id == null) {
            $tehtavaList = Tehtavat::join('teht_aika', 'teht_id', '=', 'teht_aika.aika_id')
                ->join('tilat', 'teht_id', '=', 'tilat.tila_id')
                ->select('*', 'teht_aika.paivamaara', 'tilat.tila_tila')
                ->get();

            return response()->json($tehtavaList, 200);

        } else {
            return response('No data with such id.', 404)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Adds a new note to the database.
     *
     * @param $request
     * @return Response
     */
    public function addNote(Request $request){

        $otsikko = $request->input('title');
        $paivamaara = $request->input('date');

        $otsikkoExists = Tehtavat::where('otsikko', $otsikko)
            ->first();

        if ($otsikko == null || $paivamaara == null) {
            return response('You have to fill parameters', 400)
                ->header('Content-Type', 'text/plain');

        } else if ($otsikkoExists != null){
            return response('The note with the same name already exists!', 409)
                ->header('Content-Type', 'text/plain');

        } else {
            $tehtavaAdd = Tehtavat::insertGetId(
                ['otsikko' => $otsikko]
            );

            $tilaAdd = new Tilat;
            $tilaAdd->tila_id = $tehtavaAdd;
            $tilaAdd->save();

            $aikaAdd = new Teht_aika;
            $aikaAdd->aika_id = $tehtavaAdd;
            $aikaAdd->paivamaara = $paivamaara;
            $aikaAdd->save();

            return response('New note added', 201)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Changes the note's state from not done (0) to done (1) to database if id is given.
     *
     * @param $request
     * @return Response
     *
     */
    public function updateNote($id) {

        $idExists = Tehtavat::where('teht_id', '=', $id)
            ->first();

        if ($idExists != null) {
            $note = Tilat::find($id);
            $note->tila_tila = '1';
            $note->save();

            return response('Note updated.', 206)
                ->header('Content-Type', 'text/plain');

        } else {
            return response('No data with such id.', 404)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     *
     * Deletes the note by id
     *
     * @param $request
     * @return Response
     */
    public function deleteNote($id) {

        $idExists = Tehtavat::where('teht_id', '=', $id)
            ->first();

        if($idExists != null) {
            Tehtavat::where('teht_id', '=', $id)->delete();

            Tilat::where('tila_id', '=', $id)->delete();

            Teht_aika::where('aika_id', '=', $id)->delete();

            return response('Note deleted', 200)
                ->header('Content_Type', 'text/plain');
        } else {
            return response('No data with such id.', 404)
                ->header('Content-Type', 'text/plain');
        }
    }
}
