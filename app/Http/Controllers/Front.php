<?php

namespace App\Http\Controllers;

use App\Teht_aika;
use App\Tehtavat;
use App\Tilat;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Front extends Controller
{
    var $teht_aika;
    var $tehtavat;
    var $tilat;

    public function __construct() {
        $this->teht_aika = Teht_aika::all(array('paivamaara'));
        $this->tehtavat = Tehtavat::all(array('otsikko'));
        $this->tilat = Tilat::all(array('tila_tila'));
    }

    /**
     * Brings all the data from the database to the view.
     *
     * @return Response
     *
     */
    public function index() {
        return view('tasks', array('teht_aika' => $this->teht_aika, 'tehtavat' => $this->tehtavat, 'tilat' => $this->tilat));
    }

}
