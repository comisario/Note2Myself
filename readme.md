# Note2Myself

## API documentation

### Show notes/note
    Shows all the notes or a specific note by id, title or date
* **URL**: /notes
* **Method**: GET
* **URL Params**: 
<br/> **Optional** 
<br/>`id=[integer]`, `title=[string]`, `date=[date]`
* **Data Params**: None
* **Success Response**:
    * **Code**: 200 OK<br/>
      **Content**: `[{"Teht_id":1,"Otsikko":"Homework","Aika_id":1,"Paivamaara":"2017-03-14","Tila_id":1,"Tila_tila":0,"paivamaara":"2017-03-14","tila_tila":0},{"Teht_id":2,"Otsikko":"Lahja siskolle","Aika_id":2,"Paivamaara":"2017-03-22","Tila_id":2,"Tila_tila":1,"paivamaara":"2017-03-22","tila_tila":1},{"Teht_id":3,"Otsikko":"PartyAnimals","Aika_id":3,"Paivamaara":"2017-04-01","Tila_id":3,"Tila_tila":1,"paivamaara":"2017-04-01","tila_tila":1},{"Teht_id":4,"Otsikko":"Hellou","Aika_id":4,"Paivamaara":"2017-12-24","Tila_id":4,"Tila_tila":0,"paivamaara":"2017-12-24","tila_tila":0},{"Teht_id":5,"Otsikko":"Joulukoristeet!!","Aika_id":5,"Paivamaara":"2017-11-14","Tila_id":5,"Tila_tila":1,"paivamaara":"2017-11-14","tila_tila":1},{"Teht_id":6,"Otsikko":"New age-bileet","Aika_id":6,"Paivamaara":"2017-05-30","Tila_id":6,"Tila_tila":0,"paivamaara":"2017-05-30","tila_tila":0},{"Teht_id":7,"Otsikko":"Lentoliput","Aika_id":7,"Paivamaara":"2017-10-15","Tila_id":7,"Tila_tila":0,"paivamaara":"2017-10-15","tila_tila":0},{"Teht_id":8,"Otsikko":"Vappu","Aika_id":8,"Paivamaara":"2017-05-01","Tila_id":8,"Tila_tila":1,"paivamaara":"2017-05-01","tila_tila":1},{"Teht_id":9,"Otsikko":"Moido","Aika_id":9,"Paivamaara":"2017-03-11","Tila_id":9,"Tila_tila":0,"paivamaara":"2017-03-11","tila_tila":0},{"Teht_id":10,"Otsikko":"Laundry day","Aika_id":10,"Paivamaara":"2017-09-26","Tila_id":10,"Tila_tila":0,"paivamaara":"2017-09-26","tila_tila":0},{"Teht_id":11,"Otsikko":"testi","Aika_id":11,"Paivamaara":"2020-01-01","Tila_id":11,"Tila_tila":1,"paivamaara":"2020-01-01","tila_tila":1},{"Teht_id":12,"Otsikko":"Kuulento","Aika_id":12,"Paivamaara":"2024-10-10","Tila_id":12,"Tila_tila":0,"paivamaara":"2024-10-10","tila_tila":0},{"Teht_id":13,"Otsikko":"city tour","Aika_id":13,"Paivamaara":"2022-02-01","Tila_id":13,"Tila_tila":1,"paivamaara":"2022-02-01","tila_tila":1},{"Teht_id":14,"Otsikko":"test111","Aika_id":14,"Paivamaara":"2222-02-03","Tila_id":14,"Tila_tila":1,"paivamaara":"2222-02-03","tila_tila":1},{"Teht_id":15,"Otsikko":"testing","Aika_id":15,"Paivamaara":"2333-02-03","Tila_id":15,"Tila_tila":1,"paivamaara":"2333-02-03","tila_tila":1},{"Teht_id":16,"Otsikko":"Hhhhhh","Aika_id":16,"Paivamaara":"2018-03-04","Tila_id":16,"Tila_tila":0,"paivamaara":"2018-03-04","tila_tila":0}]`
* **Error Response**:
    * **Code**: 404 Not found
    * **Content**: `No data with such id.`
    
### Add new note
    Adds a new note to the database
* **URL**: /notes/newNote
* **Method**: GET
* **URL Params**: 
<br/>**Required** <br/>`title=[string]`, `date=[date]`
* **Data Params**: None
* **Success Response**:
    * **Code**: 200 OK<br/>
      **Content**: `New note added`
* **Error Response**:
    * **Code**: 409 Conflict <br/>
      **Content**: `The note with the same name already exists!`
    * **Code**: 400 Bad Request <br/>
      **Content**: `You have to fill parameters`
      
### Update note
    Changes the note's state from not done (0) to done (1) 
    and sends the updated state to the database if id is given.
* **URL**: /notes/update/:id
* **Method**: GET
* **URL Params**: None
* **Data Params**: None
* **Success Response**:
    * **Code**: 206 Partial Content<br/>
      **Content**: `Note updated`
* **Error Response**:
    * **Code**: 404 Not found<br/> 
      **Content**: `No data with such id.`
      
### Delete note
    Deletes the note by id
* **URL**: /notes/delete/:id
* **Method**: GET
* **URL Params**: None
* **Data Params**: None
* **Success Response**:
    * **Code**: 200 OK<br/>
      **Content**: `Note deleted`
* **Error Response**:
    * **Code**: 404 Not found<br/> 
      **Content**: `No data with such id.`