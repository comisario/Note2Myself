@extends('layouts.app')

@section('content')


    <form action="{{ url('addNote') }}" method="POST">
        Otsikko<br>
        <input type="text" name="otsikko" id="otsikko"><br>
        Päivämäärä<br>
        <input type="text" name="paivamaara" id="paivamaara"><br>
        <button type="button" onclick="{{action('ApiController@addNote')}}">Lisää tehtävä</button>
        <button><a href="{{action('ApiController@addNote')}}" type="submit">Testi</a></button>
    </form>

    <table>
        <tbody>
            <tr>
                <th>Tehtävät</th>
                <th>Deadline</th>
                <th>Tila</th>
            </tr>
            @for($i = 0; $i < count($tehtavat); $i++)
                @if($tehtavat[$i]->teht_id == $teht_aika[$i]->aika_id)
                    <tr>
                        <form action="{{ url('/notes/delete/{'. $tehtavat[$i]->teht_id . '}') }}" method="POST">
                            {{ method_field('DELETE') }}
                            <td>{{ json_decode($tehtavat[$i])->otsikko }}</td>
                            <td>{{ json_decode($teht_aika[$i])->paivamaara }}</td>
                            <td>{{ json_decode($tilat[$i])->tila_tila }}</td>
                            <td><button type="submit">Poista</button></td>
                            <td><button type="submit" action="{{ url('/notes/update/' .$tehtavat[$i]->teht_id) }}" method="POST">Merkitse tehdyksi</button></td>
                        </form>
                    </tr>
                @endif
            @endfor
        </tbody>
    </table>
@endsection





