<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});

Route::get('front', 'Front@index');

Route::get('notes','ApiController@showNotes');

Route::get('/notes/newNote', 'ApiController@addNote')->name('addNote');

Route::get('/notes/update/{id}', 'ApiController@updateNote')->name('updateNote');

Route::get('/notes/delete/{id}', 'ApiController@deleteNote')->name('deleteNote');

